<?php


class RightSignature 
{
	const USER_AGENT  = 'FSM/RightSignature';
	const ROLE_SIGNER = 'signer';
	const ROLE_CC     = 'cc';

	const ACTION_SEND     = 'send';
	const ACTION_REDIRECT = 'redirect';
	const ACTION_PREFILL  = 'prefill';

	const DOCUMENT_DATA_TYPE_BASE64 = 'base64';
	const DOCUMENT_DATA_TYPE_URL    = 'url';

	const STATUS_SENT   = 'sent';
	const STATUS_SIGNED = 'signed';

	public static function url( string $uri, $params = NULL, $use_ssl = TRUE )
	{
		$url = '';
		$count = 0;
		$uri_length = strlen( $uri );

		if( $uri_length > 0 )
		{
			if( $url[ $uri_length - 1 ] == '?' ) 
			{
				$count = 1;
			}

			if( $uri[ 0 ] != '/' )
			{
				$uri = "/{$uri}";
			}
		}

		
		$url = ($use_ssl ? 'https' : 'http') . "://rightsignature.com{$uri}";

		foreach( $params as $key => $value )
		{
			if( $count++ == 0 ) $url .= '?';
			else                $url .= '&';
			

			$value = oauth_urlencode( $value );
			$url  .= "{$key}={$value}";
		}
			
		return $url;
	}

	public static function redirectTokenUrl( $redirectToken )
	{
		assert( is_string($redirectToken) && !empty($redirectToken) );
		return self::url( 'builder/new', array('rt' => $redirectToken) );
	}

	public static function requestToken( )
	{
		try
		{
			$oauth = new OAuth( RIGHTSIGNATURE_OAUTH_KEY, RIGHTSIGNATURE_OAUTH_SECRET );
			$oauth->enableDebug( );

			$request_token_url  = self::url( 'oauth/request_token' );
			$request_token_info = $oauth->getRequestToken( $request_token_url, RIGHTSIGNATURE_CALLBACK_URL );
			assert( isset($request_token_info) );

			$oauth_token                = $request_token_info[ 'oauth_token' ];
			$oauth_token_secret         = $request_token_info[ 'oauth_token_secret' ];
			$_SESSION[ 'oauth_token' ]  = $oauth_token;
			$_SESSION[ 'oauth_secret' ] = $oauth_token_secret;
		}
		catch( OAuthException $ex )
		{
			header( 'Content-Type: text/plain' );
			echo "Exception caught!\n";
			echo "Response: ". $E->lastResponse . "\n";
		}

 		$authorize_url = self::url( 'oauth/authorize', array('oauth_token' => $oauth_token) );
		header( 'Location: ' . $authorize_url );
	}

	public static function accessToken( $config_file )
	{
		if( file_exists( $config_file ) )
		{
			unlink( $config_file );
		}

		$url   = self::url( 'oauth/access_token' );
		$oauth = new OAuth( RIGHTSIGNATURE_OAUTH_KEY, RIGHTSIGNATURE_OAUTH_SECRET );
		$oauth_token        = $_SESSION[ 'oauth_token' ];
		$oauth_token_secret = $_SESSION[ 'oauth_secret' ];


		$oauth->setToken( $oauth_token, $oauth_token_secret );
		$access_token_info = $oauth->getAccessToken( $url );

		unset( $_SESSION['oauth_token'] );
		unset( $_SESSION['oauth_secret'] );

		$config  = "<?php\n";
		$config .= "define('RIGHTSIGNATURE_ACCESS_KEY', '{$access_token_info['oauth_token']}'); \n";
		$config .= "define('RIGHTSIGNATURE_ACCESS_SECRET', '{$access_token_info['oauth_token_secret']}'); \n";
		
		file_put_contents( $config_file, $config );

		header( 'Content-Type: text/plain' );
		echo "RightSignature config generated.";
	}

	public static function listDocuments( $page = 1, $per_page = 10, $tag = NULL, $recipient_email = NULL )
	{
		$arguments = array( 'page' => $page, 'per_page' => $per_page );
		if( isset($tag) )             $arguments[ 'tag' ] = $tag;
		if( isset($recipient_email) ) $arguments[ 'recipient_email' ] = $recipient_email;

		try
		{
			$url   = self::url( 'api/documents.xml', $arguments );
			$oauth = self::create_oauth( );
			self::get( $oauth, $url );

			$xml_response = simplexml_load_string( $oauth->getLastResponse( ) );
			return $xml_response;
		} 
		catch( OAuthException $E )
		{
			trigger_error( $E->getMessage( ), E_USER_ERROR );
			return FALSE;
		}
	}

	public static function documentDetails( $guid )
	{
		assert( !empty($guid) && is_string($guid) );

		try
		{
			$url   = self::url( "api/documents/{$guid}.xml" );
			$oauth = self::create_oauth( );
			self::get( $oauth, $url );

			$xml_response = simplexml_load_string( $oauth->getLastResponse( ) );
			return $xml_response;
		} 
		catch( OAuthException $E )
		{
			trigger_error( $E->getMessage( ), E_USER_ERROR );
			return FALSE;
		}
	}

	public static function batchDocumentDetails( $guids )
	{
		assert( is_array($guids) && count($guids) > 0 );

		try
		{
			$guids_list = implode(',', $guids);
			$url   = self::url( "api/documents/{$guids_list}/batch_details.xml" );
			$oauth = self::create_oauth( );
			self::get( $oauth, $url );
			
			$xml_response = simplexml_load_string( $oauth->getLastResponse( ) );
			return $xml_response;
		} 
		catch( OAuthException $E )
		{
			trigger_error( $E->getMessage( ), E_USER_ERROR );
			return FALSE;
		}
	}

    /**
     * Send a document
     * 
     * This function sends a document for signing to a list of recipients
     * 
     * $arguments is an associative array with following keys (some optional):
	 *
	 * Required arguments:
     * 
     *  - document_data        - an associative array of 'type' and 'value'
     *  - recipients           - an associative array of 'name', 'email', 'role'
     *  - sender_role          - ROLE_SIGNER, ROLE_CC
     *  - subject              - string for the subject
     *  - action               - ACTION_SEND, ACTION_REDIRECT
	 *
	 * Optional arguments:
     *  - expires_in           - 2 days, 5 days, 10 days, 30 days
     *  - description          - string for the description
     *  - tags                 - associative array of 'tag'
     *  - callback_location    - string for callback URL
     *
     * @param array $arguments
     * @return mixed (XML or FALSE)
     */
	public static function sendDocument( $arguments )
	{
		assert( array_key_exists( 'document_data', $arguments ) );
		assert( array_key_exists( 'recipients', $arguments ) );
		assert( array_key_exists( 'sender_role', $arguments ) );
		assert( array_key_exists( 'subject', $arguments ) );
		assert( array_key_exists( 'action', $arguments ) );

		
		// Build XML
		$request_xml = new SimpleXMLElement( "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n<document></document>" );
		$document_data_elem = $request_xml->addChild( "document_data" );
		$document_data_elem->addChild( "type", $arguments['document_data']['type'] );
		$document_data_elem->addChild( "value", $arguments['document_data']['value'] );
		$recipients_elem = $request_xml->addChild( "recipients" );
		foreach( $arguments['recipients'] as $recipient_data )
		{
			$recipient_elem = $recipients_elem->addChild( "recipient" );
			$recipient_elem->addChild( "name", $recipient_data['name'] );
			$recipient_elem->addChild( "email", $recipient_data['email'] );
			$recipient_elem->addChild( "role", $recipient_data['role'] );
		}
		$request_xml->addChild( "sender_role", $arguments['sender_role'] );
		$request_xml->addChild( "subject", $arguments['subject'] );
		$request_xml->addChild( "action", $arguments['action'] );
		if( array_key_exists( 'expires_in', $arguments ) )
		{
			$request_xml->addChild( "expires_in", "{$arguments['expires_in']} days" );
		}
		if( array_key_exists( 'description', $arguments ) )
		{
			$request_xml->addChild( "description", $arguments['description'] );
		}
		if( array_key_exists( 'tags', $arguments ) )
		{
			$tags_elem = $request_xml->addChild( "tags" );
			foreach( $arguments['tags'] as $tag_data )
			{
				$tag_elem = $tags_elem->addChild( "tag" );
				$tags_elem->addChild( "value", $tag_data['value'] );
			}
		}
		if( array_key_exists( 'callback_location', $arguments ) )
		{
			$request_xml->addChild( "callback_location", $arguments['callback_location'] );
		}

		try
		{
			$url   = "api/documents.xml";
			$oauth = self::create_oauth( );
			self::post( $oauth, $url, $request_xml->asXML( ) );
			
			$xml_response = simplexml_load_string( $oauth->getLastResponse( ) );
			return $xml_response;
		} 
		catch( OAuthException $E )
		{
			trigger_error( $E->getMessage( ), E_USER_ERROR );
			return FALSE;
		}
	}

	public static function listTemplates( $tag = NULL )
	{
		$arguments = array();
		if( isset($tag) )
		{
			$arguments = array( 'tag' => $tag );
		}

		try
		{
			$url   = self::url( "api/templates.xml", $arguments );
			$oauth = self::create_oauth( );
			self::get( $oauth, $url );
		
			$xml_response = simplexml_load_string( $oauth->getLastResponse( ) );
			return $xml_response;
		} 
		catch( Exception $E )
		{
			trigger_error( $E->getMessage( ), E_USER_ERROR );
			return FALSE;
		}
	}

	public static function templateDetails( $guid )
	{
		assert( is_string($guid) && !empty($guid) );

		try
		{
			$url   = self::url( "api/templates/{$guid}.xml" );
			$oauth = self::create_oauth( );
			self::get( $oauth, $url );

			$xml_response = simplexml_load_string( $oauth->getLastResponse( ) );
			return $xml_response;
		} 
		catch( OAuthException $E )
		{
			trigger_error( $E->getMessage( ), E_USER_ERROR );
			return FALSE;
		}
	}

	public static function prepackageTemplate( $guid_or_guids, $callback_location = NULL )
	{
		if( is_array($guid_or_guids) )
		{ 
			assert( count($guid_or_guids) > 0 );
			$guid_or_guids = implode( ',', $guid_or_guids );
		}
		if( is_string($guid_or_guids) )
		{
			assert( !empty($guid_or_guids) );
		}

		$request_xml = '';

		if( isset($callback_location) ) 
		{
			$request_xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n<callback_location> {$callback_location} </callback_location>";
		}

		try
		{
			$url   = "api/templates/{$guid_or_guids}/prepackage.xml";
			$oauth = self::create_oauth( );
			self::post( $oauth, $url, $request_xml );

			$response_xml = simplexml_load_string( $oauth->getLastResponse( ) );
			return $response_xml;
		} 
		catch( OAuthException $E )
		{
			trigger_error( $E->getMessage( ), E_USER_ERROR );
			return FALSE;
		}
	}

	/**
	 *
	 *	Arguments:
	 *		guid
	 *		subject
	 *		description
	 *		expires_in
	 *		roles
	 *		merge_fields
	 *		tags
	 *		action
	 *
	 */
	public static function prefillTemplate( &$arguments )
	{
		if( !array_key_exists( 'expires_in', $arguments) ) $arguments['expires_in'] = 5;
		if( !array_key_exists( 'action', $arguments) )     $arguments['action']     = self::ACTION_PREFILL;

		// Build XML
		$request_xml = new SimpleXMLElement( "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n<template></template>" );
		$request_xml->addChild( 'guid', $arguments['guid'] );
		//if( array_key_exists( 'subject', $arguments ) )      $request_xml->addChild( "subject", "<![CDATA[{$arguments['subject']}]]>" );
		//if( array_key_exists( 'description', $arguments ) )  $request_xml->addChild( "description", "<![CDATA[{$arguments['description']}]]>" );
		if( array_key_exists( 'subject', $arguments ) )      $request_xml->addChild( "subject", htmlentities($arguments['subject']) );
		if( array_key_exists( 'description', $arguments ) )  $request_xml->addChild( "description", htmlentities($arguments['description']) );
		if( array_key_exists( 'expires_in', $arguments ) )   $request_xml->addChild( "expires_in", "{$arguments['expires_in']} days" );
		$request_xml->addChild( 'action', $arguments['action'] );

		if( count($arguments['roles']) > 0 )
		{
			$roles_elem = $request_xml->addChild( "roles" );
			foreach( $arguments['roles'] as $role )
			{
				$role_elem = $roles_elem->addChild( "role" );
				if( array_key_exists( 'role_name', $role) ) $role_elem->addAttribute( "role_name", $role['role_name'] ); 
				if( array_key_exists( 'role_id', $role) )   $role_elem->addAttribute( "role_id", $role['role_id'] ); 
				$role_elem->addChild( "name", $role['name'] );
				$role_elem->addChild( "email", $role['email'] );
				if( array_key_exists( 'must-sign', $role) )    $role_elem->addChild( "must-sign", intval($role['must-sign']) ? 'true' : 'false' );
				if( array_key_exists( 'locked', $role) )    $role_elem->addChild( "locked", intval($role['locked']) ? 'true' : 'false' );
			}
		}
		if( count($arguments['merge_fields']) > 0 )
		{
			$merge_fields_elem = $request_xml->addChild( "merge_fields" );
			foreach( $arguments['merge_fields'] as $merge_field )
			{
				$merge_field_elem = $merge_fields_elem->addChild( "merge_field" );
				if( array_key_exists( 'id', $merge_field ) )      $merge_field_elem->addAttribute( "merge_field_id", $merge_field['id'] ); 
				if( array_key_exists( 'name', $merge_field ) )    $merge_field_elem->addAttribute( "merge_field_name", $merge_field['name'] ); 
				if( array_key_exists( 'locked', $merge_field ) )  $merge_field_elem->addChild( "locked", $merge_field['locked'] ? 'true' : 'false' );
				//$merge_field_elem->addChild( "value", "<![CDATA[{$merge_field['value']}]]>" );
				$merge_field_elem->addChild( "value", htmlentities($merge_field['value']) );
			}
		}
		if( array_key_exists( 'tags', $arguments ) )
		{
			$tags_elem = $request_xml->addChild( "tags" );
			foreach( $arguments['tags'] as $tag_data )
			{
				$tag_elem = $tags_elem->addChild( "tag" );
				$tag_elem->addChild( "value", $tag_data['value'] );
			}
		}
		if( array_key_exists( 'callback_location', $arguments ) )
		{
			$request_xml->addChild( "callback_location", $arguments['callback_location'] );
		}


		try
		{
			$url   = "api/templates.xml";
			$oauth = self::create_oauth( );
			self::post( $oauth, $url, $request_xml->asXML( ) );
			$response_xml = simplexml_load_string( $oauth->getLastResponse( ) );
			return $response_xml;
		} 
		catch( OAuthException $E )
		{
			trigger_error( $E->getMessage( ), E_USER_ERROR );
			return FALSE;
		}
	}

	public static function sendTemplate( $template_guid, &$arguments )
	{
		assert( isset($template_guid) );
		assert( is_array($arguments) );

		$package = self::prepackageTemplate( $template_guid );

		if( $package )
		{
			$arguments[ 'guid' ]   = strval( $package->guid );
			$arguments[ 'action' ] = self::ACTION_SEND;
			
			return self::prefillTemplate( $arguments );
		}

		return FALSE;
	}

	public static function createTemplate( $arguments = NULL )
	{
		try
		{
			$url   = self::url( 'api/templates/new.xml', $arguments );
			$oauth = self::create_oauth( );
			self::get( $oauth, $url );
			
			$response_xml = simplexml_load_string( $oauth->getLastResponse( ) );
			return $response_xml;
		} 
		catch( OAuthException $E )
		{
			trigger_error( $E->getMessage( ), E_USER_ERROR );
			return FALSE;
		}
	}

	public static function cloneTemplate( $guid )
	{
		assert( !empty($guid) );

		try
		{
			$url   = self::url( "api/account/{$guid}/clone_template.xml" );
			$oauth = self::create_oauth( );
			self::get( $oauth, $url );
			
			$xml_response = simplexml_load_string( $oauth->getLastResponse( ) );
			return $xml_response;
		} 
		catch( OAuthException $E )
		{
			trigger_error( $E->getMessage( ), E_USER_ERROR );
			return FALSE;
		}
	}

	public static function addUser( $name, $email )
	{
		assert( !empty($name) );
		assert( !empty($email) );

		// Build XML
		$request_xml = new SimpleXMLElement( "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n<user></user>" );
		$request_xml->addChild( "name", $name );
		$request_xml->addChild( "email", $email );

		try
		{
			$url   = "api/users.xml";
			$oauth = self::create_oauth( );
			self::post( $oauth, $url, $request_xml->asXML( ) );
			
			$xml_response = simplexml_load_string( $oauth->getLastResponse( ) );
			return $xml_response;
		} 
		catch( OAuthException $E )
		{
			trigger_error( $E->getMessage( ), E_USER_ERROR );
			return FALSE;
		}
	}

	const USAGE_SINCE_MONTH = 'month';
	const USAGE_SINCE_WEEK  = 'week';
	const USAGE_SINCE_DAY   = 'day';

	public static function usageReport( $since = NULL, $only_signed = FALSE )
	{
		$arguments = array();

		if( isset($since) )
		{
			$arguments['since'] = $since;
		}
		if( $only_signed )
		{
			$arguments['signed'] = 'true';
		}

		try
		{
			$url   = self::url( "api/account/usage_report.xml", $arguments );
			$oauth = self::create_oauth( );
			self::get( $oath, $url );

			$xml_response = simplexml_load_string( $oauth->getLastResponse( ) );
			return $xml_response;
		} 
		catch( OAuthException $E )
		{
			trigger_error( $E->getMessage( ), E_USER_ERROR );
			return FALSE;
		}
	}

	private static function create_oauth( )
	{
		$oauth = new OAuth( RIGHTSIGNATURE_OAUTH_KEY, RIGHTSIGNATURE_OAUTH_SECRET, OAUTH_SIG_METHOD_HMACSHA1, OAUTH_AUTH_TYPE_URI );
		$oauth->setToken( RIGHTSIGNATURE_ACCESS_KEY, RIGHTSIGNATURE_ACCESS_SECRET );
		$oauth->enableDebug( );
		
		return $oauth;
	}

	private static function get( &$oauth, $url )
	{
		$headers = array( 
			'User-Agent' => self::USER_AGENT,
		);
		$oauth->fetch( $url, NULL, OAUTH_HTTP_METHOD_GET, $headers );
		$response_info = $oauth->getLastResponseInfo( );
		assert( strpos( $response_info["content_type"], 'xml' ) !== FALSE );
	}

	private static function post( &$oauth, string $url, string $xml )
	{
		// JoeM: I kept this around because as a reference for other OAuth APIs that use multipart/form-data.
		//$mime_boundary = "FSM" . md5( time( ) );
		//$headers       = array( "Content-Type" => "multipart/form-data; boundary={$mime_boundary}" );
		//$body  = "--{$mime_boundary}\r\n";
		//$body .= "Content-Disposition: form-data; name=\"xml\"; filename=\"post.xml\"\r\n";
		//$body .= "Content-Type: application/xml; charset=utf-8\r\n";
		//$body .= "\r\n";
		//$body .= $xml;
		//$body .= "\r\n";
		//$body .= "--{$mime_boundary}--";
		//$oauth->fetch( $url, $body, OAUTH_HTTP_METHOD_POST, $headers );
		//$body = "xml=" . oauth_urlencode( $xml );

        $oauth_body_hash = self::hash( $xml );
		$url             = self::url( $url, array( 'oauth_body_hash' => $oauth_body_hash ) );	
	
		$headers = array( 
			'User-Agent'    => self::USER_AGENT,
			'Content-Type'  => "application/xml; charset=utf-8",
		);
		$oauth->fetch( $url, $xml, OAUTH_HTTP_METHOD_POST, $headers );
		$response_info = $oauth->getLastResponseInfo( );
		assert( strpos( $response_info["content_type"], 'xml' ) !== FALSE );
	}

	private static function hash( $data )
	{
    	return base64_encode(
            hash_hmac(
                'sha1', 
				$data,
				self::getHashKey( ),
				true
            )
        );
	}

	private static function getHashKey( )
	{
		return oauth_urlencode(RIGHTSIGNATURE_OAUTH_SECRET) . '&' . oauth_urlencode(RIGHTSIGNATURE_ACCESS_SECRET);
	}


	private static function debug_xml( $xml )
	{
		header( 'Content-Type: application/xml' );
		if( is_object($request_xml) )
			echo $request_xml->asXML();
		else
			echo strval($xml);
		die();

	}


	// Get the posted XML.  This is useful for RightSignature callbacks.
	public static function requestXml( )
	{
		$xml = simplexml_load_file( 'php://input' );
		return $xml;
	}


	public static function xml2Array($arrObjData, $arrSkipIndices = array(), $replace_dashes = TRUE )
	{
		$arrData = array();

		// if input is object, convert into array
		if (is_object($arrObjData)) {
			$arrObjData = get_object_vars($arrObjData);
		}

		if (is_array($arrObjData)) {
			foreach ($arrObjData as $index => $value) {
				if (is_object($value) || is_array($value)) {
					$value = self::xml2Array($value, $arrSkipIndices); // recursive call
				}
				if (in_array($index, $arrSkipIndices)) {
					continue;
				}
				$index = str_replace( '-', '_', $index );
				$arrData[$index] = $value;
			}
		}
		return $arrData;
	}

}

