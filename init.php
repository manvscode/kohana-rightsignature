<?php defined('SYSPATH') or die('No direct script access.');


if( !defined( 'MODULE_RIGHTSIGNATURE' ) )
{
	$config = Kohana::$config->load( 'rightsignature' );

	define( 'RIGHTSIGNATURE_ACCESS_KEY', $config->get('api.access_key') );
	define( 'RIGHTSIGNATURE_ACCESS_SECRET', $config->get('api.access_secret') );

	
	define( 'MODULE_RIGHTSIGNATURE', TRUE );
}

